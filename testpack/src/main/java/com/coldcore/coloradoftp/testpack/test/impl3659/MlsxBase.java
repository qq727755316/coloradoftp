package com.coldcore.coloradoftp.testpack.test.impl3659;

import com.coldcore.coloradoftp.testpack.test.BaseCommunicatorTest;
import com.coldcore.misc5.StringReaper;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Base class.
 */
abstract public class MlsxBase extends BaseCommunicatorTest {

  protected Set<String> facts;


  public MlsxBase() {
    facts = new HashSet<String>();
    facts.add("modify");
    facts.add("type");
    facts.add("perm");
    facts.add("size");
  }


  /**
   * Test if a path item is listed in a server's reply
   * @param reply Server's reply
   * @param item Path item
   * @param dir TRUE if item is a directory, FALSE if item is a file
   * @param size Expected item's size (file only)
   * @param date Expected item's date
   * @param facts Expected item's facts
   */
  protected void ensureItemIsListed(String reply, String item, boolean dir, long size, Date date, Set<String> facts) throws Exception {
    StringReaper sr = new StringReaper(reply);
    String[] val = sr.getValues(null, "\r\n");
    int ind = 0;
    boolean mlst = reply.startsWith("250-");
    for (String s : val) {
      if (++ind == 1 && mlst) continue;
      if (!s.endsWith(" "+item)) continue;
      sr.setContent(s.trim());
      val = sr.getValues(null, " ");
      if (val[0].startsWith("/")) {
        if (facts.isEmpty()) return;
        if (dir && facts.size() == 1 && facts.contains("size")) return;
        fail("Item '"+item+"' facts failed: "+reply);
      }

      String ifacts = val[0];
      StringTokenizer st = new StringTokenizer(ifacts, ";");
      Map<String,String> pmap = new HashMap<String,String>();
      while (st.hasMoreTokens()) {
        String token = st.nextToken();
        if (token.indexOf("=") == -1 || token.indexOf(" ") != -1) fail("Item '"+item+"' facts format failed: "+reply);
        pmap.put(token.substring(0,token.indexOf("=")).toLowerCase(), token.substring(token.indexOf("=")+1));
      }

      for (String fact : facts) {
        if (dir && fact.equals("size")) continue;
        String pval = pmap.get(fact);
        if (pval == null) fail("Item '"+item+"' fact '"+fact+"' failed: "+reply);

        if (fact.equals("size") && !pval.equals(""+size)) fail("Item '"+item+"' size failed: "+reply);
        if (fact.equals("perm") && pval.equals("")) fail("Item '"+item+"' perm failed: "+reply);
        if (fact.equals("type") && !dir && !pval.equals("file")) fail("Item '"+item+"' type failed: "+reply);
        if (fact.equals("type") && dir && !pval.equals("dir") && !pval.equals("cdir") && !pval.equals("pdir")) fail("Item '"+item+"' type failed: "+reply);
        if (fact.equals("modify")) checkItemDate(pval, date);
      }

      return;
    }
    fail("Item '"+item+"' not listed: "+reply);
  }


  /**
   * Test if a list in a server's reply has expected lines count
   * @param reply Server's reply
   * @param count Expected lines count
   */
  protected void ensureListLineCountMatches(String reply, int count) {
    StringReaper sr = new StringReaper(reply);
    String[] val = sr.getValues(null, "\r\n");
    if (count != val.length) fail("Listing line count does not match ("+count+"/"+val.length+"): "+reply);
  }


  /**
   * Test if date matches the expected date
   * @param dt Date string
   * @param date Expected date
   */
  protected void checkItemDate(String dt, Date date) throws Exception {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmm");
    sdf.setTimeZone(TimeZone.getTimeZone("GMT"));

    if (dt.indexOf(".") != -1) dt = dt.substring(0, dt.indexOf("."));

    Calendar cal = new GregorianCalendar();
    cal.setTime(date);
    int year = cal.get(Calendar.YEAR);

    Date itemDate = sdf.parse(dt);
    cal.setTime(itemDate);
    cal.set(Calendar.YEAR, year);
    cal.add(Calendar.MINUTE, 2);
    if (cal.getTime().compareTo(date) < 0) fail("Item date failed: "+dt);
    cal.add(Calendar.MINUTE, -3);
    if (cal.getTime().compareTo(date) < 0) fail("Item date failed: "+dt);
  }
}
