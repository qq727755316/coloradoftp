package com.coldcore.coloradoftp.testpack.test.impl3659;

import org.apache.log4j.Logger;

import java.util.Date;

/**
 * Test if server properly lists items.
 * Testing MLST, MLSD commands.
 * This test assumes that directories in server response are in absolute form with
 * additional current and parent directories.
 */
public class MlsxListing extends MlsxBase {

  private static Logger log = Logger.getLogger(MlsxListing.class);


  protected void setUp() throws Exception {
    beansFilename = "impl3659-beans.xml";
    super.setUp();
  }


  public void testListing() throws Exception {
    command("mlsd", "530");
    command("mlst", "530");

    anonymousLogin();

    command("mkd testdir-1", "257", "250");
    command("mkd testdir-2", "257", "250");
    command("mkd testdir-2/testdir-3", "257", "250");
    command("mkd testdir-2/testdir-4", "257", "250");

    createRemoteTextFile(112, remoteUsersPath+"/anonymous/testfile.txt", false);
    createRemoteTextFile(1024, remoteUsersPath+"/anonymous/testdir-1/testfile-1.txt", false);
    createRemoteTextFile(256, remoteUsersPath+"/anonymous/testdir-1/testfile-2.txt", false);
    createRemoteTextFile(16, remoteUsersPath+"/anonymous/testdir-1/testfile-3.txt", false);
    createRemoteTextFile(1024, remoteUsersPath+"/anonymous/testdir-2/testfile-A.txt", false);
    createRemoteTextFile(256, remoteUsersPath+"/anonymous/testdir-2/testdir-3/testfile-B.txt", false);
    createRemoteTextFile(32003, remoteUsersPath+"/anonymous/testdir-2/testdir-3/testfile-C.txt", false);

    Date curdate = new Date();
    log.info("Using date "+curdate);

    String reply;


    pasv();
    command("mlsd", "150");
    reply = pasvTransfer.readDataAsString();
    dataTransferComplete();
    ensureItemIsListed(reply, "/testfile.txt", false, 112, curdate, facts);
    ensureItemIsListed(reply, "/testdir-1", true, 0, curdate, facts);
    ensureItemIsListed(reply, "/testdir-2", true, 0, curdate, facts);
    ensureItemIsListed(reply, "/", true, 0, curdate, facts);
    log.info("Listing OK");

    reply = command("mlst", "250");
    ensureItemIsListed(reply, "/", true, 0, curdate, facts);
    ensureListLineCountMatches(reply, 3);
    log.info("Listing OK");

    reply = command("mlst testdir-1", "250");
    ensureItemIsListed(reply, "/testdir-1", true, 0, curdate, facts);
    log.info("Listing OK");

    reply = command("mlst testfile.txt", "250");
    ensureItemIsListed(reply, "/testfile.txt", false, 112, curdate, facts);
    log.info("Listing OK");


    command("cwd testdir-1", "250");

    pasv();
    command("mlsd", "150");
    reply = pasvTransfer.readDataAsString();
    dataTransferComplete();
    ensureItemIsListed(reply, "/testdir-1/testfile-1.txt", false, 1024, curdate, facts);
    ensureItemIsListed(reply, "/testdir-1/testfile-2.txt", false, 256, curdate, facts);
    ensureItemIsListed(reply, "/testdir-1/testfile-3.txt", false, 16, curdate, facts);
    ensureItemIsListed(reply, "/", true, 0, curdate, facts);
    ensureItemIsListed(reply, "/testdir-1", true, 0, curdate, facts);
    ensureListLineCountMatches(reply, 5);
    log.info("Listing OK");

    command("mlsd testfile-2.txt", "450");

    reply = command("mlst", "250");
    ensureItemIsListed(reply, "/testdir-1", true, 0, curdate, facts);
    ensureListLineCountMatches(reply, 3);
    log.info("Listing OK");

    reply = command("mlst testfile-2.txt", "250");
    ensureItemIsListed(reply, "/testdir-1/testfile-2.txt", false, 256, curdate, facts);
    ensureListLineCountMatches(reply, 3);
    log.info("Listing OK");


    command("cwd ../testdir-2", "250");

    pasv();
    command("mlsd", "150");
    reply = pasvTransfer.readDataAsString();
    dataTransferComplete();
    ensureItemIsListed(reply, "/testdir-2/testfile-A.txt", false, 1024, curdate, facts);
    ensureItemIsListed(reply, "/testdir-2/testdir-3", true, 0, curdate, facts);
    ensureItemIsListed(reply, "/testdir-2/testdir-4", true, 0, curdate, facts);
    ensureItemIsListed(reply, "/", true, 0, curdate, facts);
    ensureItemIsListed(reply, "/testdir-2", true, 0, curdate, facts);
    ensureListLineCountMatches(reply, 5);
    log.info("Listing OK");

    reply = command("mlst", "250");
    ensureItemIsListed(reply, "/testdir-2", true, 0, curdate, facts);
    ensureListLineCountMatches(reply, 3);
    log.info("Listing OK");

    reply = command("mlst testfile-A.txt", "250");
    ensureItemIsListed(reply, "/testdir-2/testfile-A.txt", false, 1024, curdate, facts);
    ensureListLineCountMatches(reply, 3);
    log.info("Listing OK");


    command("cwd testdir-3", "250");

    pasv();
    command("mlsd", "150");
    reply = pasvTransfer.readDataAsString();
    dataTransferComplete();
    ensureItemIsListed(reply, "/testdir-2/testdir-3/testfile-B.txt", false, 256, curdate, facts);
    ensureItemIsListed(reply, "/testdir-2/testdir-3/testfile-C.txt", false, 32003, curdate, facts);
    ensureItemIsListed(reply, "/testdir-2", true, 0, curdate, facts);
    ensureItemIsListed(reply, "/testdir-2/testdir-3", true, 0, curdate, facts);
    ensureListLineCountMatches(reply, 4);
    log.info("Listing OK");

    reply = command("mlst", "250");
    ensureItemIsListed(reply, "/testdir-2/testdir-3", true, 0, curdate, facts);
    ensureListLineCountMatches(reply, 3);
    log.info("Listing OK");

    reply = command("mlst testfile-C.txt", "250");
    ensureItemIsListed(reply, "/testdir-2/testdir-3/testfile-C.txt", false, 32003, curdate, facts);
    ensureListLineCountMatches(reply, 3);
    log.info("Listing OK");

    reply = command("mlst testfile-B.txt", "250");
    ensureItemIsListed(reply, "/testdir-2/testdir-3/testfile-B.txt", false, 256, curdate, facts);
    ensureListLineCountMatches(reply, 3);
    log.info("Listing OK");


    command("cwd ../testdir-4", "250");

    pasv();
    command("mlsd", "150");
    reply = pasvTransfer.readDataAsString();
    dataTransferComplete();
    ensureItemIsListed(reply, "/testdir-2", true, 0, curdate, facts);
    ensureItemIsListed(reply, "/testdir-2/testdir-4", true, 0, curdate, facts);
    ensureListLineCountMatches(reply, 2);
    log.info("Listing OK");

    reply = command("mlst", "250");
    ensureItemIsListed(reply, "/testdir-2/testdir-4", true, 0, curdate, facts);
    ensureListLineCountMatches(reply, 3);
    log.info("Listing OK");


    logout();
  }


  public void testListingSubDir() throws Exception {
    anonymousLogin();

    command("mkd testdir-1", "257", "250");
    command("mkd testdir-2", "257", "250");
    command("mkd testdir-2/testdir-3", "257", "250");
    command("mkd testdir-2/testdir-4", "257", "250");

    createRemoteTextFile(112, remoteUsersPath+"/anonymous/testfile.txt", false);
    createRemoteTextFile(1024, remoteUsersPath+"/anonymous/testdir-1/testfile-1.txt", false);
    createRemoteTextFile(256, remoteUsersPath+"/anonymous/testdir-1/testfile-2.txt", false);
    createRemoteTextFile(16, remoteUsersPath+"/anonymous/testdir-1/testfile-3.txt", false);
    createRemoteTextFile(1024, remoteUsersPath+"/anonymous/testdir-2/testfile-A.txt", false);
    createRemoteTextFile(256, remoteUsersPath+"/anonymous/testdir-2/testdir-3/testfile-B.txt", false);
    createRemoteTextFile(32003, remoteUsersPath+"/anonymous/testdir-2/testdir-3/testfile-C.txt", false);

    Date curdate = new Date();
    log.info("Using date "+curdate);
    String reply;

    command("mlsd crazy", "450");
    command("mlst crazy", "450");

    pasv();
    command("mlsd /", "150");
    reply = pasvTransfer.readDataAsString();
    dataTransferComplete();
    ensureItemIsListed(reply, "/testfile.txt", false, 112, curdate, facts);
    ensureItemIsListed(reply, "/testdir-1", true, 0, curdate, facts);
    ensureItemIsListed(reply, "/testdir-2", true, 0, curdate, facts);
    ensureItemIsListed(reply, "/", true, 0, curdate, facts);
    log.info("Listing OK");

    reply = command("mlst /", "250");
    ensureItemIsListed(reply, "/", true, 0, curdate, facts);
    ensureListLineCountMatches(reply, 3);
    log.info("Listing OK");

    reply = command("mlst /testdir-1", "250");
    ensureItemIsListed(reply, "/testdir-1", true, 0, curdate, facts);
    ensureListLineCountMatches(reply, 3);
    log.info("Listing OK");

    reply = command("mlst /testfile.txt", "250");
    ensureItemIsListed(reply, "/testfile.txt", false, 112, curdate, facts);
    ensureListLineCountMatches(reply, 3);
    log.info("Listing OK");


    pasv();
    command("mlsd testdir-1", "150");
    reply = pasvTransfer.readDataAsString();
    dataTransferComplete();
    ensureItemIsListed(reply, "/testdir-1/testfile-1.txt", false, 1024, curdate, facts);
    ensureItemIsListed(reply, "/testdir-1/testfile-2.txt", false, 256, curdate, facts);
    ensureItemIsListed(reply, "/testdir-1/testfile-3.txt", false, 16, curdate, facts);
    ensureItemIsListed(reply, "/", true, 0, curdate, facts);
    ensureItemIsListed(reply, "/testdir-1", true, 0, curdate, facts);
    ensureListLineCountMatches(reply, 5);
    log.info("Listing OK");

    command("mlsd testdir-1/testfile-3.txt", "450");

    reply = command("mlst testdir-1", "250");
    ensureItemIsListed(reply, "/testdir-1", true, 0, curdate, facts);
    ensureListLineCountMatches(reply, 3);
    log.info("Listing OK");

    reply = command("mlst testdir-1/testfile-3.txt", "250");
    ensureItemIsListed(reply, "/testdir-1/testfile-3.txt", false, 16, curdate, facts);
    ensureListLineCountMatches(reply, 3);
    log.info("Listing OK");


    command("cwd testdir-1", "250");

    pasv();
    command("mlsd ../testdir-2", "150");
    reply = pasvTransfer.readDataAsString();
    dataTransferComplete();
    ensureItemIsListed(reply, "/testdir-2/testfile-A.txt", false, 1024, curdate, facts);
    ensureItemIsListed(reply, "/testdir-2/testdir-3", true, 0, curdate, facts);
    ensureItemIsListed(reply, "/testdir-2/testdir-4", true, 0, curdate, facts);
    ensureItemIsListed(reply, "/", true, 0, curdate, facts);
    ensureItemIsListed(reply, "/testdir-2", true, 0, curdate, facts);
    ensureListLineCountMatches(reply, 5);
    log.info("Listing OK");

    reply = command("mlst ../testdir-2", "250");
    ensureItemIsListed(reply, "/testdir-2", true, 0, curdate, facts);
    ensureListLineCountMatches(reply, 3);
    log.info("Listing OK");

    reply = command("mlst ../testdir-2/testfile-A.txt", "250");
    ensureItemIsListed(reply, "/testdir-2/testfile-A.txt", false, 1024, curdate, facts);
    ensureListLineCountMatches(reply, 3);
    log.info("Listing OK");


    pasv();
    command("mlsd /testdir-2/testdir-3", "150");
    reply = pasvTransfer.readDataAsString();
    dataTransferComplete();
    ensureItemIsListed(reply, "/testdir-2/testdir-3/testfile-B.txt", false, 256, curdate, facts);
    ensureItemIsListed(reply, "/testdir-2/testdir-3/testfile-C.txt", false, 32003, curdate, facts);
    ensureItemIsListed(reply, "/testdir-2", true, 0, curdate, facts);
    ensureItemIsListed(reply, "/testdir-2/testdir-3", true, 0, curdate, facts);
    ensureListLineCountMatches(reply, 4);
    log.info("Listing OK");

    reply = command("mlst /testdir-2/testdir-3", "250");
    ensureItemIsListed(reply, "/testdir-2/testdir-3", true, 0, curdate, facts);
    ensureListLineCountMatches(reply, 3);
    log.info("Listing OK");

    reply = command("mlst /testdir-2/testdir-3/testfile-B.txt", "250");
    ensureItemIsListed(reply, "/testdir-2/testdir-3/testfile-B.txt", false, 256, curdate, facts);
    ensureListLineCountMatches(reply, 3);
    log.info("Listing OK");

    reply = command("mlst /testdir-2/testdir-3/testfile-C.txt", "250");
    ensureItemIsListed(reply, "/testdir-2/testdir-3/testfile-C.txt", false, 32003, curdate, facts);
    ensureListLineCountMatches(reply, 3);
    log.info("Listing OK");


    command("cwd /", "250");

    pasv();
    command("mlsd testdir-2/testdir-4", "150");
    reply = pasvTransfer.readDataAsString();
    dataTransferComplete();
    ensureItemIsListed(reply, "/testdir-2", true, 0, curdate, facts);
    ensureItemIsListed(reply, "/testdir-2/testdir-4", true, 0, curdate, facts);
    ensureListLineCountMatches(reply, 2);
    log.info("Listing OK");

    reply = command("mlst testdir-2/testdir-4", "250");
    ensureItemIsListed(reply, "/testdir-2/testdir-4", true, 0, curdate, facts);
    log.info("Listing OK");


    command("mlsd testdir-2/testdir-4/crazy", "450");
    command("mlst testdir-2/testdir-4/crazy", "450");


    logout();
  }


  public void testListingSubDirSpace() throws Exception {
    anonymousLogin();

    command("mkd testdir-2 [bolt]", "257", "250");
    command("mkd testdir-2 [bolt]/testdir-3 [bolt]", "257", "250");
    command("mkd testdir-2 [bolt]/testdir-4 [bolt]", "257", "250");

    createRemoteTextFile(1024, remoteUsersPath+"/anonymous/testdir-2 [bolt]/testfile-A [bolt].txt", false);

    Date curdate = new Date();
    log.info("Using date "+curdate);
    String reply;


    pasv();
    command("mlsd testdir-2 [bolt]", "150");
    reply = pasvTransfer.readDataAsString();
    dataTransferComplete();
    ensureItemIsListed(reply, "/testdir-2 [bolt]/testfile-A [bolt].txt", false, 1024, curdate, facts);
    ensureItemIsListed(reply, "/testdir-2 [bolt]/testdir-3 [bolt]", true, 0, curdate, facts);
    ensureItemIsListed(reply, "/testdir-2 [bolt]/testdir-4 [bolt]", true, 0, curdate, facts);
    ensureItemIsListed(reply, "/", true, 0, curdate, facts);
    ensureItemIsListed(reply, "/testdir-2 [bolt]", true, 0, curdate, facts);
    ensureListLineCountMatches(reply, 5);
    log.info("Listing OK");

    reply = command("mlst testdir-2 [bolt]", "250");
    ensureItemIsListed(reply, "/testdir-2 [bolt]", true, 0, curdate, facts);
    ensureListLineCountMatches(reply, 3);
    log.info("Listing OK");

    reply = command("mlst testdir-2 [bolt]/testfile-A [bolt].txt", "250");
    ensureItemIsListed(reply, "/testdir-2 [bolt]/testfile-A [bolt].txt", false, 1024, curdate, facts);
    ensureListLineCountMatches(reply, 3);
    log.info("Listing OK");


    pasv();
    command("mlsd testdir-2 [bolt]/testdir-4 [bolt]", "150");
    reply = pasvTransfer.readDataAsString();
    dataTransferComplete();
    ensureItemIsListed(reply, "/testdir-2 [bolt]", true, 0, curdate, facts);
    ensureItemIsListed(reply, "/testdir-2 [bolt]/testdir-4 [bolt]", true, 0, curdate, facts);
    ensureListLineCountMatches(reply, 2);
    log.info("Listing OK");

    reply = command("mlst testdir-2 [bolt]/testdir-4 [bolt]", "250");
    ensureItemIsListed(reply, "/testdir-2 [bolt]/testdir-4 [bolt]", true, 0, curdate, facts);
    log.info("Listing OK");


    logout();
  }

}
