package com.coldcore.coloradoftp.testpack.test.genericbundle;

import com.coldcore.coloradoftp.testpack.test.BaseCommunicatorTest;
import com.coldcore.misc5.StringReaper;

import java.util.Date;
import java.util.Locale;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.text.SimpleDateFormat;

import org.apache.log4j.Logger;

/**
 * Test if server properly lists items.
 * Testing STAT commands with argument only
 * @see com.coldcore.coloradoftp.testpack.test.genericbundle.Listing
 */
public class StatListing extends BaseCommunicatorTest {

  private static Logger log = Logger.getLogger(StatListing.class);


  protected void setUp() throws Exception {
    beansFilename = "genericbundle-beans.xml";
    super.setUp();
  }


  public void testListingSubDir() throws Exception {
    anonymousLogin();

    command("mkd testdir-1", "257", "250");
    command("mkd testdir-2", "257", "250");
    command("mkd testdir-2/testdir-3", "257", "250");
    command("mkd testdir-2/testdir-4", "257", "250");

    createRemoteTextFile(112, remoteUsersPath+"/anonymous/testfile.txt", false);
    createRemoteTextFile(1024, remoteUsersPath+"/anonymous/testdir-1/testfile-1.txt", false);
    createRemoteTextFile(256, remoteUsersPath+"/anonymous/testdir-1/testfile-2.txt", false);
    createRemoteTextFile(16, remoteUsersPath+"/anonymous/testdir-1/testfile-3.txt", false);
    createRemoteTextFile(1024, remoteUsersPath+"/anonymous/testdir-2/testfile-A.txt", false);
    createRemoteTextFile(256, remoteUsersPath+"/anonymous/testdir-2/testdir-3/testfile-B.txt", false);
    createRemoteTextFile(32003, remoteUsersPath+"/anonymous/testdir-2/testdir-3/testfile-C.txt", false);

    Date curdate = new Date();
    log.info("Using date "+curdate);
    String reply;

    command("stat crazy", "450");

    reply = command("stat /", "212");
    if (!reply.contains("testfile.txt\r\n") || !reply.contains("testdir-1\r\n") || !reply.contains("testdir-2\r\n")) fail("Listing NO: "+reply);


    reply = command("stat testdir-1", "212");
    ensureListItem(reply, "testfile-1.txt", false, 1024, curdate);
    ensureListItem(reply, "testfile-2.txt", false, 256, curdate);
    ensureListItem(reply, "testfile-3.txt", false, 16, curdate);
    ensureListLineCount(reply, 3+2);
    log.info("Listing OK");


    command("cwd testdir-1", "250");

    reply = command("stat ../testdir-2", "212");
    ensureListItem(reply, "testfile-A.txt", false, 1024, curdate);
    ensureListItem(reply, "testdir-3", true, 0, curdate);
    ensureListItem(reply, "testdir-4", true, 0, curdate);
    ensureListLineCount(reply, 3+2);
    log.info("Listing OK");


    reply = command("stat /testdir-2/testdir-3", "212");
    ensureListItem(reply, "testfile-B.txt", false, 256, curdate);
    ensureListItem(reply, "testfile-C.txt", false, 32003, curdate);
    ensureListLineCount(reply, 2+2);
    log.info("Listing OK");


    command("cwd /", "250");

    reply = command("stat testdir-2/testdir-4", "212");
    ensureListLineCount(reply, 0+2);
    log.info("Listing OK");


    command("stat testdir-2/crazy", "450");

    logout();
  }


  public void testListingSubDirSpace() throws Exception {
    anonymousLogin();

    command("mkd testdir-2 [bolt]", "257", "250");
    command("mkd testdir-2 [bolt]/testdir-3 [bolt]", "257", "250");
    command("mkd testdir-2 [bolt]/testdir-4 [bolt]", "257", "250");

    createRemoteTextFile(1024, remoteUsersPath+"/anonymous/testdir-2 [bolt]/testfile-A [bolt].txt", false);

    Date curdate = new Date();
    log.info("Using date "+curdate);
    String reply;


    reply = command("stat testdir-2 [bolt]", "212");
    ensureListItem(reply, "testfile-A [bolt].txt", false, 1024, curdate);
    ensureListItem(reply, "testdir-3 [bolt]", true, 0, curdate);
    ensureListItem(reply, "testdir-4 [bolt]", true, 0, curdate);
    ensureListLineCount(reply, 3+2);
    log.info("Listing OK");


    reply = command("stat testdir-2 [bolt]/testdir-4 [bolt]", "212");
    ensureListLineCount(reply, 0+2);
    log.info("Listing OK");

    logout();
  }


  private void ensureListItem(String reply, String item, boolean dir, long size, Date date) throws Exception {
    StringReaper sr = new StringReaper(reply);
    String[] val = sr.getValues(null, "\r\n");
    SimpleDateFormat sdf = new SimpleDateFormat("MMM dd HH:mm", new Locale("en"));
    Calendar cal = new GregorianCalendar();
    cal.setTime(date);
    int year = cal.get(Calendar.YEAR);
    for (String s : val) {
      if (!s.endsWith(" "+item)) continue;
      sr.setContent(s.trim());
      val = sr.getValues(null, " ");
      if (val.length < 8) fail("List item '"+item+"' failed: "+reply);
      String perm = val[0];
      String len = val[3];
      String dt = val[4]+" "+val[5]+" "+val[6];
      if (dir && perm.charAt(0) != 'd') fail("List item '"+item+"' type failed: "+reply);
      if (!dir && perm.charAt(0) != '-') fail("List item '"+item+"' type failed: "+reply);
      if (!dir && Long.parseLong(len) != size) fail("List item '"+item+"' size failed: "+reply);
      Date itemDate = sdf.parse(dt);
      cal.setTime(itemDate);
      cal.set(Calendar.YEAR, year);
      cal.add(Calendar.MINUTE, 2);
      if (cal.getTime().compareTo(date) < 0) fail("List item '"+item+"' date failed: "+reply);
      cal.add(Calendar.MINUTE, -4);
      if (cal.getTime().compareTo(date) > 0) fail("List item '"+item+"' date failed: "+reply);
      if (!item.startsWith(val[7])) fail("List item '"+item+"' name failed: "+reply);
      return;
    }
    fail("Item '"+item+"' not listed: "+reply);
  }


  private void ensureListLineCount(String reply, int count) {
    StringReaper sr = new StringReaper(reply);
    String[] val = sr.getValues(null, "\r\n");
    if (count != val.length) fail("Listing line count does not match ("+count+"/"+val.length+"): "+reply);
  }

}
