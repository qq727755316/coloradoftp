package com.coldcore.coloradoftp.testpack.test.impl3659;

import org.apache.log4j.Logger;
import com.coldcore.coloradoftp.testpack.test.BaseCommunicatorTest;

import java.util.Date;
import java.util.TimeZone;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.text.SimpleDateFormat;

/**
 * Test simple MDTM, SIZE, TVFS commands.
 */
public class SimpleCommands extends BaseCommunicatorTest {

  private static Logger log = Logger.getLogger(SimpleCommands.class);


  protected void setUp() throws Exception {
    beansFilename = "impl3659-beans.xml";
    super.setUp();
  }


  public void testSize() throws Exception {
    command("size", "530");

    anonymousLogin();

    String reply;

    command("mkd testdir-1", "257", "250");
    command("mkd testdir-2 [bolt]", "257", "250");

    createRemoteBinaryFile(55678, remoteUsersPath+"/anonymous/testfile.dat");
    createRemoteBinaryFile(1172532, remoteUsersPath+"/anonymous/testdir-1/testfile-A.dat");
    createRemoteBinaryFile(112233, remoteUsersPath+"/anonymous/testdir-2 [bolt]/testfile-B [bolt].dat");

    command("size", "501");
    command("size crazy", "550");
    command("size testfile.dat", "550");

    switchToBinaryMode();

    command("size crazy", "450");

    reply = command("size testfile.dat", "213");
    if (!reply.equals("213 55678\r\n")) fail("File size NO: "+reply);
    log.info("File size OK");

    reply = command("size testdir-1/testfile-A.dat", "213");
    if (!reply.equals("213 1172532\r\n")) fail("File size NO: "+reply);
    log.info("File size OK");

    reply = command("size /testdir-2 [bolt]/testfile-B [bolt].dat", "213");
    if (!reply.equals("213 112233\r\n")) fail("File size NO: "+reply);
    log.info("File size OK");

    command("size testdir-1", "550");


    logout();
  }


  public void testMdtm() throws Exception {
    command("mdtm", "530");

    anonymousLogin();

    String reply;

    command("mkd testdir-1", "257", "250");
    command("mkd testdir-2 [bolt]", "257", "250");

    createRemoteBinaryFile(55678, remoteUsersPath+"/anonymous/testfile.dat");
    createRemoteBinaryFile(1172532, remoteUsersPath+"/anonymous/testdir-1/testfile-A.dat");
    createRemoteBinaryFile(112233, remoteUsersPath+"/anonymous/testdir-2 [bolt]/testfile-B [bolt].dat");

    Date curdate = new Date();
    log.info("Using date "+curdate);

    command("mdtm", "501");
    command("mdtm crazy", "450");

    reply = command("mdtm testfile.dat", "213");
    checkItemDate(reply.substring(4), curdate);
    log.info("Item date OK");

    reply = command("mdtm testdir-1/testfile-A.dat", "213");
    checkItemDate(reply.substring(4), curdate);
    log.info("Item date OK");

    reply = command("mdtm /testdir-2 [bolt]/testfile-B [bolt].dat", "213");
    checkItemDate(reply.substring(4), curdate);
    log.info("Item date OK");

    command("mdtm testdir-1", "550");


    logout();
  }


  public void testFeatTvfs() throws Exception {
    String reply;

    reply = command("feat tvfs", "211");
    if (!reply.contains("\r\n TVFS\r\n")) fail("FEAT TVSF NO: "+reply);
    log.info("FEAT TVSF OK");
  }


  /**
   * Test if date matches the expected date
   * @param dt Date string
   * @param date Expected date
   */
  private void checkItemDate(String dt, Date date) throws Exception {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmm");
    sdf.setTimeZone(TimeZone.getTimeZone("GMT"));

    if (dt.indexOf(".") != -1) dt = dt.substring(0, dt.indexOf("."));

    Calendar cal = new GregorianCalendar();
    cal.setTime(date);
    int year = cal.get(Calendar.YEAR);

    Date itemDate = sdf.parse(dt);
    cal.setTime(itemDate);
    cal.set(Calendar.YEAR, year);
    cal.add(Calendar.MINUTE, 2);
    if (cal.getTime().compareTo(date) < 0) fail("Item date failed: "+dt);
    cal.add(Calendar.MINUTE, -3);
    if (cal.getTime().compareTo(date) < 0) fail("Item date failed: "+dt);
  }
}
