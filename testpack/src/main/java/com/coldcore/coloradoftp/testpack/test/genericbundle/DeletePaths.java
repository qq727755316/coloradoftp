package com.coldcore.coloradoftp.testpack.test.genericbundle;

import com.coldcore.coloradoftp.testpack.test.BaseCommunicatorTest;
import org.apache.log4j.Logger;

/**
 * Test if server properly deletes files with DELE command.
 */
public class DeletePaths extends BaseCommunicatorTest {

  private static Logger log = Logger.getLogger(DeletePaths.class);


  protected void setUp() throws Exception {
    beansFilename = "genericbundle-beans.xml";
    super.setUp();
  }


  public void testDele() throws Exception {
    command("dele", "530");

    anonymousLogin();

    createRemoteBinaryFile(60456, remoteUsersPath+"/anonymous/testfile-1.dat");
    createRemoteBinaryFile(72552, remoteUsersPath+"/anonymous/testfile-2.dat");

    ensureListedInWorkingDirectory("testfile-1.dat", "testfile-2.dat");

    command("dele", "501");
    command("dele testfile-A", "450");

    command("dele testfile-1.dat", "250");
    ensureNotListedInWorkingDirectory("testfile-1.dat");
    ensureListedInWorkingDirectory("testfile-2.dat");

    logout();
  }


  public void testDeleSubdirs() throws Exception {

    anonymousLogin();

    command("mkd testdir-1", "257", "250");
    command("mkd testdir-2", "257", "250");
    command("mkd testdir-2/testdir-3", "257", "250");

    createRemoteBinaryFile(60456, remoteUsersPath+"/anonymous/testdir-1/testfile-1.dat");
    createRemoteBinaryFile(72552, remoteUsersPath+"/anonymous/testdir-2/testfile-2.dat");
    createRemoteBinaryFile(51246, remoteUsersPath+"/anonymous/testdir-2/testfile-3.dat");
    createRemoteBinaryFile(99373, remoteUsersPath+"/anonymous/testdir-2/testfile-4.dat");
    createRemoteBinaryFile(12435, remoteUsersPath+"/anonymous/testdir-2/testdir-3/testfile-5.dat");

    command("cwd /", "250");
    ensureWorkingDirectoryMatches("/");
    command("dele testdir-1/testfile-1.dat", "250");
    command("cwd testdir-1", "250");
    ensureWorkingDirectoryMatches("/testdir-1");
    ensureNothingListedInWorkingDirectory();

    command("cwd /", "250");
    ensureWorkingDirectoryMatches("/");
    command("dele /testdir-2/testfile-2.dat", "250");
    command("cwd testdir-2", "250");
    ensureWorkingDirectoryMatches("/testdir-2");
    ensureNotListedInWorkingDirectory("testfile-2");
    ensureListedInWorkingDirectory("testfile-3", "testfile-4");

    command("dele ../testdir-2/../testdir-2/testfile-3.dat", "250");
    ensureNotListedInWorkingDirectory("testfile-2", "testfile-3");
    ensureListedInWorkingDirectory("testfile-4");

    command("cwd testdir-3", "250");
    command("dele testfile-5.dat", "250");
    ensureNothingListedInWorkingDirectory();

    command("dele ../testfile-4.dat", "250");
    command("cdup", "250");
    ensureWorkingDirectoryMatches("/testdir-2");
    ensureNotListedInWorkingDirectory("testfile-4.dat");

    logout();
  }

}
