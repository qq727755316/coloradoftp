package com.coldcore.coloradoftp.testpack.test.example;

import com.coldcore.coloradoftp.testpack.test.BaseClientTest;

/**
 * Example tests.
 */
public class SimpleExample extends BaseClientTest {


  /** Using higher-level methods of FTP client */
  public void testLoginLogout() throws Exception {
    assertFalse("crazy/crazy login did not fail", ftpClient.login("crazy", "crazy"));
    assertTrue("Anonymous login failed", ftpClient.login("anonymous", "aaa@aa.aa"));
    assertTrue("Logout failed", ftpClient.logout());
  }

}